export const CONFIG = {
    JWT_TOKEN: "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NTYxMywiZW1haWwiOiJkZXZlbG9wZXJhcGlAeW9wbWFpbC5jb20iLCJtb2JpbGVfbm8iOiIzNDUzNjQ1NTY0IiwiZmlyc3RfbmFtZSI6IkRldmVsb3AiLCJsYXN0X25hbWUiOiJBUEkiLCJyb2xlcyI6IjExIiwicm9sZU5hbWVzIjoiRGV2ZWxvcGVyIiwiYXZhdGFyIjoiYXNzZXRzL2ltYWdlcy9hdmF0YXJzL3Byb2ZpbGUuanBnIiwidmlkZW9faW50cm9fdXJsIjpudWxsLCJtb29kIjoiSSBhbSBPbiIsInN0YXR1cyI6Im9mZmxpbmUiLCJ2b2lwX251bWJlciI6bnVsbCwic21zX251bWJlcl90d2lsbGlvIjpudWxsLCJleHQiOm51bGwsImh1YnNwb3RfaWQiOm51bGwsInNpZ25hdHVyZSI6bnVsbCwiaWF0IjoxNTY2Mjg2MjQxfQ.s35qSV9jESImTma584t9znoUgrUQIA-HwRjHq8NoCr0",
    jobDiva: {
        "clientid": "215",
        "username": "jdapi@aptask.com",
        "password": "Yealink@123",
        "initialURL": "http://ws.jobdiva.com/axis2/services/BIData/getBIData"
    },
    EMAIL_TEMPLATES: {
        "email template 1": {
            id:1,
            content: "<div><p>Hello <b>{{Candidate_First_Name}}</b>,</p><p>Blessings for the day!!</p><p>My name is <b>{{Recruiter_First_Name}}</b> with ApTask. Your profile appears to be a best match to one of my current openings and I would love to discuss this exciting opportunity with you. Below is the job description.</p><p> Let me know if interested.</p><br/><br/><b>{{Job_Description}}</b><br/><p>Please click here to answer brief questions and apply to this job: <b>{{BOTURL}}</b>.</p><span style='color:#2e5395'>Thanks & Regards</span><br/><br/><span style='color:#2e5395'>Signature of Recruiter</span></div>"
        },
        "email template 2": {
            id:2,
            content:"<div><p>Hi <b>{{Candidate_First_Name}}</b>,</p><p>My name is <b>{{Recruiter_First_Name}}</b> and I'm an IT recruiter withApTask. I came across your profile while searching for qualified candidates.<br/>See the job details below and let me know if you or someone you know may be interested, reply with yourupdated resume and expected salary.</p><br/><br/><b>{{Job_Description}}</b><br/><br/><p>Please click here to answer brief questions and apply to this job: <b>{{BOTURL}}</b>.</p><span style='color:#2e5395'>Thanks & Regards</span><br/><br/><span style='color:#2e5395'>Signature of Recruiter</span></div>"
        },
        "email template 3": {
            id:3,
            content:"<div><p>Hello <b>{{Candidate_First_Name}}</b>,</p><p>I hope you are doing great! This is <b>{{Recruiter_First_Name}}</b> (Talent Acquisition Team) from ApTask. If you are the one who would love to be a part of fortune 500 clients, please reach me at <b>{{Recruiter_Contact_Number}}</b>. I might be the one who can help you with your next career move.If you are not looking for opportunities now, you can still connect with me and help me to understand the kind of jobs you are looking for. I can definitely help you in meeting your dream job. If not, we will approach you in the future at the right time.</p><br/><br/><b>{{Job_Description}}</b><br/><br/><p>Please click here to answer brief questions and apply to this job: <b>{{BOTURL}}</b>.</p><span style='color:#2e5395'>Thanks & Regards</span><br/><br/><span style='color:#2e5395'>Signature of Recruiter</span></div>"
        }
    }    
}