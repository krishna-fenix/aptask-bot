import { Router } from '@angular/router/';
import { StoreValuesService } from "./../services/store-values.service";
import { BaseService } from "./../services/base.service";
import {
  Component,
  OnInit,
  AfterViewChecked,
  AfterViewInit,
  ElementRef,
  ViewChild
} from "@angular/core";
import * as $ from "jquery";
import { CONFIG } from "../../config";
import { MatDialog } from "@angular/material";
import { AddCandidateComponent } from "../add-candidate/add-candidate.component";
import { ActivatedRoute } from "@angular/router";
import { AddDetailsComponent } from '../add-details/add-details.component';

@Component({
  selector: "app-chat-window",
  templateUrl: "./chat-window.component.html",
  styleUrls: ["./chat-window.component.css"]
})
export class ChatWindowComponent
	implements OnInit, AfterViewInit, AfterViewChecked {
	public htmlToAdd;
	userResponse = "";
	sessionId: any;
	apiResponse: any;
	responseButtondata: any;
	showLoader: Boolean = false;
	public count = 1;
	userCount = 1;
	storeURL: any;
	toggleButton: boolean = false;
	addParameter: {};
	calendly_link: any;
	avatar_link: any
	hidebubble:boolean = true;
	constructor(
		public baseService: BaseService,
		public storeValues: StoreValuesService,
		private dialog: MatDialog,
		private activeRoute: Router
	) {
		this.storeURL = this.baseService.getJSON();
		console.log("storeURL****" + JSON.stringify(this.baseService.getJSON()));
	}
	scaleResponse: any;
	checkBoxResponse: string[] = [];
	today = new Date();
	amOrPm = this.today.getHours() < 12 ? "AM" : "PM";
	time = this.today.getHours() + ":" + this.today.getMinutes() + " " + this.amOrPm;
	recruiterDetails = {};
	candidateDetails = {};
	totalDetails = {};
	emailTemplate: any;
	emails = [];
	isResponseTextEmpty:boolean = false;
	apiRecalled:boolean = false;
	botSuccess = false;
	botError = false;
	// ipObj = {rid: 1, cid: 11331720862965, bid: 1};
	// uploadedFileBase64:string = "";

	@ViewChild("scrollMe") private myScrollContainer: ElementRef;

	ngOnInit() {
		// this.baseService.getJSON().subscribe(data => {
		//   console.log('json .ts' + JSON.stringify(data['point']));
		// });

		this.showLoader = true;
		if (window.localStorage.getItem("cid") == "undefined") {
			this.openAddCandidateDialog();
		} else {
			console.log("id found =>")
		}
		this.sessionId = new Date().getTime();
		this.getRecruiterDetails(window.localStorage.getItem("rid"));
		for (const key in CONFIG.EMAIL_TEMPLATES) {
			if (
				CONFIG.EMAIL_TEMPLATES[key].id == window.localStorage.getItem("etid")
			) {
				this.emailTemplate = CONFIG.EMAIL_TEMPLATES[key];
			}
		}

		// let businessDevRoute = this.activeRoute.url;
		// var res = businessDevRoute.match(/bd/);

		const queryString = window.location.search;
		const urlParams = new URLSearchParams(queryString);
		const bd = urlParams.get('bd')
		if (bd == '2') {
			this.openAddCandidateDialog();
		}
	}

	ngAfterViewInit() {
		this.baseService.getJSON().subscribe(data => {
			this.storeURL = data["end_point"];
			console.log("storeurl" + this.storeURL);
			if (this.storeURL) {
				this.scrollToBottom();
			}
		});
	}

	ngAfterViewChecked() {
		this.scrollToBottom();
	}

  	// enter presss key function
	keyDownFunction(event) {
		if (event.keyCode === 13) {
			this.onClickSend();
		}
	}

  	firstCall() {
		console.log("oasbdibasidbsaoifoysabfuy@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
		var dataString = "Hi";
		var url = window.location.href;
		var queryString = url.split("?");
		if (queryString[1] != undefined)
			dataString = queryString[1].replace(/=/g, ": ").replace(/&/g, ", ");

		this.baseService.getFirstResponse(this.sessionId, dataString, this.totalDetails).subscribe(
			data => {
				this.apiResponse = data;
				this.hidebubble = true;
				console.log("s9dhf9s9f7sc7f8h27h77ng9x6xc6349xc673gnmf", this.apiResponse)
			}
		);
	}

  	getRecruiterDetails(rid) {
		console.log(" getRecruiterDetails => ")
		this.baseService.getRecruiterDetails(rid).subscribe(
			data => {
				console.log("-----Recruiter Success----");
				console.log(data.data);
				this.recruiterDetails["id"] = data.data.user_id;
				this.recruiterDetails["name"] = data.data.name;
				this.recruiterDetails["email"] = data.data.email;
				this.recruiterDetails["mobile"] = data.data.voip_number;
				this.recruiterDetails["sms_number"] = data.data.sms_number_twillio;
				this.recruiterDetails["calendly_link"] = data.data.calendly_link;
				this.recruiterDetails["avatar_link"] = data.data.avatar;
				this.getCandidateProfileById(window.localStorage.getItem("cid"));
			},
			error => {
				console.log("-----Recruiter Errror----");
				console.log(error);
			}
		);
	}

	getCandidateProfileById(cand_id) {
		console.log("searching candidate....");
		this.baseService.getCandidateDetails(cand_id).subscribe(
			response => {
				console.log(response.data.respData);
				this.candidateDetails["id"] = response.data.respData.ID;
				this.candidateDetails["name"] =
				response.data.respData.FIRSTNAME +
				" " +
				response.data.respData.LASTNAME;
				this.candidateDetails["email"] = response.data.respData.EMAIL;
				this.welcomeFunction();
			},
			error => {
				console.log(error);
			}
		);
	}

  	// onclick send button function
 	onClickSend() {
		if(!this.isResponseTextEmpty){
			let enterdata = $('<div class="message-container right">').append(
			$('<div class="response-wrap" id="response-wrap">').append(
				$('<div class="response" id="response">').append(
				$('<div class="text" id="userTextSend">')
					.append($('<p style="margin:0;">').text(this.userResponse))
					.append(
					$(
						'<span class="mat-time">' +
						this.time +
						' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'
					)
					)
				)
			)
			);
			enterdata.attr("id", "right" + this.userCount);

			$(".append").after(enterdata);
			$(".append").remove();
			$('<div class="append">').insertAfter($("#right" + this.userCount));
			this.userCount = this.userCount + 1;
		}
		if(!this.apiRecalled){
			this.baseService.getResponse(this.sessionId, this.userResponse).subscribe(
				response => {
					if(response['data'].text == ''){
						this.isResponseTextEmpty = true;
						this.apiRecalled = true;
						this.onClickSend();
					}else{
						this.userResponse = "";
						this.hidebubble = true;
						this.apiRecalled = false;
						this.isResponseTextEmpty = false;
						this.generateHTML(response);
					}
				}
			);
		}else{
			this.hidebubble = true;
			this.botError = true;
		}
	}
	reloadBot(){
		window.location.reload()
	}
  	welcomeFunction() {
		var dataString = "Hi";
		var url = window.location.href;
		this.hidebubble = false;
		var queryString = url.split("?");
		// var ipData = queryString[1].split("&");
		// for(let i=0;i<ipData.length;i++){
		// 	var tempArr = ipData[i].split("=");
		// 	this.ipObj[tempArr[0]] = tempArr[1];
		// }
		// this.candidateDetails = {
		// 	id:this.ipObj.cid,
		// 	name: "Shubham Solanki",
		// 	email: "shubham@fenixwork.com",
		// 	mobile: "9588405567"
		// };
		// this.recruiterDetails = {
		// 	id: this.ipObj.rid,
		// 	name: "Super Admin",
		// 	email: "superadmin@aptask.com",
		// 	mobile: "8014500506"
		// };
		// this.getCompleteDetails(this.ipObj.rid);

		this.totalDetails = {
			candidateDetails: this.candidateDetails,
			recruiterDetails: this.recruiterDetails,
			emailTemplate: this.emailTemplate
		};

		const bdData = window.location.search;
		const urlParams = new URLSearchParams(bdData);
		const bd = urlParams.get('bd')
		if (bd == '2') {
			console.log("++++++++++++++++++++++++++++++++++++++")
			this.addParameter = {
				name: "bddata",
				parameters: this.totalDetails
			}
		} else if (bd == undefined) {
			this.addParameter = {
				name: "data",
				parameters: this.totalDetails
			}
		}

    	console.log("Recruiter details: ", JSON.stringify(this.recruiterDetails));

		// this.getRecruiterByRid(this.ipObj.rid);
		if (queryString[1] != undefined)
			dataString = queryString[1].replace(/=/g, ": ").replace(/&/g, ", ");
		console.log("welcome***" + this.storeURL);
		this.baseService.getFirstResponse(this.sessionId, dataString, this.addParameter).subscribe(
			data => {
			this.showLoader = false;
			this.apiResponse = data;
			this.hidebubble = true;
			console.log("################################################################", this.apiResponse.data.text)
			if (this.apiResponse.data.text != "") {
				console.log("datavdv**" + this.apiResponse.text);
				this.generateHTML(this.apiResponse);
			} else {
				this.baseService.getFirstResponse(this.sessionId, dataString, this.addParameter).subscribe(data => {
					this.generateHTML(data);
				});
			}
		});
	}

  // getCompleteDetails(rid){
  // 	console.log("call recruiter 2.0...")
  // 	var url = "http://pulse.aptask.com:7050/api/2.0/users/getuserbyid?userid="+rid;
  // 	this.baseService.getRecruiterDetails(url).subscribe(
  // 		data => {
  // 			console.log("-----Recruiter Success 2.0----");
  // 			this.recruiterDetails = {
  // 				id: rid,
  // 				name: data.data.name,
  // 				email: data.data.email,
  // 				mobile: data.data.mobile_no
  // 			};
  // 			// this.RName = data.data.name;
  // 			// this.REmail = data.data.email;
  // 			// this.RMobile = data.data.mobile_no;
  // 		},
  // 		error => {
  // 			console.log("-----Recruiter Errror----");
  // 			console.log(error);
  // 		}
  // 	)
  // }

  generateWelcomeMessgae(welcomeResponse) {
		const welcomedata = $(
			'<div class="message-container" id="welcome-container">'
			).append(
			$('<div class="message type-1 passed">').append(
				$('<div class="avatar-container" id="firstavatar">')
			)
		);

		$("#message-box").append(welcomedata);
		$('<div class="append">').insertAfter($("#welcome-container"));
		const responseDiv = document.getElementById("firstavatar");
		for (let i = 0; i < welcomeResponse.data.text.length; i++) {
			console.log("in loop " + welcomeResponse.data.text[i]);
			responseDiv.innerHTML +=
				'<div class="question" id="question"> <div class="text">' +
				welcomeResponse.data.text[i] +
				"</div> </div>";
		}
  }

  generateHTML(response) {
    var isTextPrinted = false;
    var skip = false;
	var skip_btn;
	
    if (response.data.hasOwnProperty("skip")) {
		var that = this;
		if (response.data.skip) {
			skip_btn = $(
			'<div><button type="button" id="skip-it-' +
			this.count +
			'" class="skip_action">Skip</button></div>'
			);
			$(document).on("click", "#skip-it-" + this.count, function () {
				that.skipQuestion(that);
				that.hidebubble = false;
				$('.ex-salary-input').attr('readonly',true).val('');
				$(this).css('pointer-events','none');
			});
		}
    }

    if (response.data.text && !response.data.buttons && Object.keys(response.data.payload).length === 0) {
		console.log("in text");
		this.toggleButton = true;
		var userDataDiv = $('<div class="avatar-container" id="avatar">');
		for (let i = 0; i < response.data.text.length; i++) {
			userDataDiv.append(
			$('<div class="question" id="que">').append(
				$('<div class="text">')
				.append($('<p style="margin:0;">').text(response.data.text[i]))
				.append(
					$(
					'<span class="mat-time">' +
					this.time +
					' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'
					)
				)
			)
			);
		}
		const userText = document.querySelector("#userRes");
		const htmldata = $('<div class="message-container">').append(
			$('<div class="message type-1 passed">')
			.append(userDataDiv)
			.append(skip_btn)
		);
		htmldata.attr("id", "message-container" + this.count);
		$(".append").after(htmldata);
		$(".append").remove();
		$('<div class="append">').insertAfter(
			$("#message-container" + this.count)
		);
		$("#message-box").append($("#message-container"));
		this.count = this.count + 1;
		console.log("con inc" + this.count);
		console.log("acc response!!!!!!!!!!!!!!!!!!!!!" + JSON.stringify(response));
		isTextPrinted = true;
    }

    if (response.data.text && response.data.buttons) {
		console.log("in text with button");
		this.toggleButton = false;
		var userDataDiv = $('<div class="avatar-container" id="avatar">');
		for (let i = 0; i < response.data.text.length; i++) {
			userDataDiv.append(
			$('<div class="question" id="que">').append(
				$('<div class="text">')
				.append($('<p style="margin:0;">').text(response.data.text[i]))
				.append(
					$(
					'<span class="mat-time">' +
					this.time +
					' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'
					)
				)
			)
			);
		}

		var userButton = $(
			'<div class="actions-container" id="actionContainer">'
		);
		var demovar = "";
		var buttonClickNew = this.buttonClick;
		var that = this;
		for (let i = 0; i < response.data.buttons.length; i++) {
			console.log("in loop " + response.data.buttons[i]);

			userButton.append(
			$(' <div class="wrap">').append(
				$(
				'<div tabindex="1" class="action" id="actionButton' +
				i +
				"-" +
				this.count +
				'" >'
				).append($("<span>").text(response.data.buttons[i]))
			)
			);
			demovar = response.data.buttons[i];
			$(document).on("click","#actionButton" + i + "-" + this.count,function () {
				demovar = response.data.buttons[i];
				buttonClickNew(demovar, that);
				that.hidebubble = false;
				$('.action').css('pointer-events','none');
				$('.skip_action').css('pointer-events','none');
				console.log(demovar);
			});
		}

		const userText = document.querySelector("#userRes");
		const htmldata = $('<div class="message-container">').append(
			$('<div class="message type-1 passed">')
			.append(userDataDiv)
			.append(userButton)
			.append(skip_btn)
		);
		htmldata.attr("id", "message-container" + this.count);
		$(".append").after(htmldata);
		$(".append").remove();
		$('<div class="append">').insertAfter(
			$("#message-container" + this.count)
		);
		$("#message-box").append($("#message-container"));

		this.count = this.count + 1;
		console.log("con inc" + this.count);

		console.log("acc response" + JSON.stringify(response));
		isTextPrinted = true;
    }

    if (response.data.text && response.data.payload && Object.keys(response.data.payload).length > 0) {
		this.toggleButton = true;
		var userDataDiv = $('<div class="avatar-container" id="avatar">');
		for (let i = 0; i < response.data.text.length; i++) {
			userDataDiv.append(
			$('<div class="question" id="que">').append(
				$('<div class="text">')
				.append($('<p style="margin:0;">').text(response.data.text[i]))
				.append(
					$(
					'<span class="mat-time">' +
					this.time +
					' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'
					)
				)
			)
			);
		}

		// Check Payload Option
		if (response.data.payload.hasOwnProperty("Calender")) {
			console.log("render mat datepicker");
			var userDatePicker = $(
			'<div class="actions-container" id="actionContainer">'
			);
			var dateInput = $(
			'<input type="date" class="date-picker-input" id="dateInput-' +
			this.count +
			'">'
			);
			var that = this;
			$(document).on("change", "#dateInput" + "-" + this.count, function (
			event
			) {
				that.userResponse = this.value;
				that.hidebubble = false;
				$('.skip_action').css('pointer-events','none');
				$(this).attr('readonly',true);
				that.onClickSend();
			});
			userDatePicker.append($(' <div class="wrap">').append(dateInput));

			const htmldata = $('<div class="message-container">').append(
			$('<div class="message type-1 passed">')
				.append(userDataDiv)
				.append(userDatePicker)
				.append(skip_btn)
			);
			htmldata.attr("id", "message-container" + this.count);
			$(".append").after(htmldata);
			$(".append").remove();
			$('<div class="append">').insertAfter(
			$("#message-container" + this.count)
			);
			$("#message-box").append($("#message-container"));

			this.count = this.count + 1;
		} else if (response.data.payload.hasOwnProperty("textBox")) {
			var userTextBox = $('<div class="actions-container input-action" id="actionContainer">');
			var that = this;
			var long = (response.data.payload.long) ? '300px': '100px';
			for (let i = 0; i < response.data.payload.textBox.length; i++) {
				console.log("in loop " + response.data.payload.textBox[i]);

				if (response.data.payload.textBox[i].key.indexOf("+") > -1) {
					var splitByPlus = response.data.payload.textBox[i].key.split("+");
					var placeholder = (response.data.payload.textBox[i].placeholder) ? response.data.payload.textBox[i].placeholder : "Enter Value";
					userTextBox.append(
					$(' <div class="wrap">').append(
						$('<div tabindex="1" class="expect-salary-action" id="actionButton' + i +"-" +this.count +'" >')
						.append($("<span>" +splitByPlus[0] +'</span> <input type="textbox" style="width:'+long+'" placeholder="'+placeholder+'" class="date-picker-input ex-salary-input" id="inputButton' +i +"-" +this.count +'"> <span>' +splitByPlus[1] +"</span>"))
					)
					);

					// $(document).on("keypress","#inputButton" + i + "-" + this.count,function (event) {
					// 	if (event.keyCode == 13) {
					// 		console.log("value from salary input");
					// 		console.log(event);
					// 		$(this).attr('readonly',true);
					// 		that.hidebubble = false;
					// 		that.userResponse = splitByPlus[0] + this.value + splitByPlus[1];
					// 		that.userResponse = (that.userResponse == '') ? "Skip": that.userResponse;
					// 		$(this).parent().parent().parent().find('.skip_button').css('pointer-events','none');
					// 		$('.skip_action').css('pointer-events','none');
					// 		$(this).val('');
					// 		that.onClickSend();
					// 	}
					// });

					userTextBox.append($('<button type="button" class="skip_action" id="salary-submit-btn-'+this.count+'">Submit</button>'));
					$(document).on("click","#salary-submit-btn-"+this.count,function (event) {
						
						console.log("value from salary input");
						var value = $(this).parent().find("input").val();
						that.hidebubble = false;
						that.userResponse = splitByPlus[0] + value + splitByPlus[1];
						that.userResponse = (that.userResponse == '') ? "Skip": that.userResponse;
						$(this).css('pointer-events','none');
						$('.skip_action').css('pointer-events','none');
						$('.ex-salary-input').attr('readonly',true).val('');
						that.onClickSend();
						
					});
				}
			}
			
			const htmldata = $('<div class="message-container">').append(
			$('<div class="message type-1 passed">')
				.append(userDataDiv)
				.append(userTextBox)
				.append(skip_btn)
			);
			htmldata.attr("id", "message-container" + this.count);
			$(".append").after(htmldata);
			$(".append").remove();
			$('<div class="append">').insertAfter($("#message-container" + this.count));
			$("#message-box").append($("#message-container"));

			this.count = this.count + 1;
		} else if (response.data.payload.hasOwnProperty("dropDown")) {
			console.log("render Drop Downs");
		} else if (response.data.payload.hasOwnProperty("scale")) {
			console.log("render scale");

			var userSlider = $(
			'<div class="actions-container" id="actionContainer">'
			);
			var wrap = $('<div class="wrap">');
			var actions = $(
			'<div tabindex="1" class="scale-action" id="actionButton-' +
			this.count +
			'" >'
			);
			var that = this;
			this.scaleResponse = response.data.payload.scale;
			for (const key in response.data.payload.scale) {
			if (response.data.payload.scale.hasOwnProperty(key)) {
				var element = response.data.payload.scale[key];
				var range = element.split(" to ");
				actions.append(
				$(
					'<div class="slidecontainer"><div style="margin:0;text-transform: uppercase;">' +
					key +
					': <span class="span-fill" data-value="' +
					key +
					'" id="demo-' +
					this.count +
					'"></span><div class="divFlex"><div class="pm-0">0</div><div class="pm-0">10</div></div></div><input type="range" min="' +
					range[0] +
					'" max="' +
					range[1] +
					'" value="0" class="slider" id="range-slider-' +
					this.count +
					'"></div>'
				)
				);

				$(document).on("input", "#range-slider-" + this.count, function () {
				that.scaleResponse[
					$(this)
					.parent()
					.find(".span-fill")
					.attr("data-value")
				] = this.value;
				$(this)
					.parent()
					.find("span")
					.text(this.value);
				});
			}
			this.count = this.count + 1;
			}
			wrap.append(actions);
			userSlider
			.append(wrap)
			.append(
				$(
				'<button type="button" class="action" id="scale-submission">Submit</button>'
				)
			);

			$(document).on("click", "#scale-submission", function () {
				$(this).parent().find("span").text(this.value);
				for (const key in that.scaleResponse) {
					that.userResponse += key + ": " + that.scaleResponse[key] + ",";
				}
				that.userResponse = that.userResponse.substring(0,that.userResponse.length - 1);
				$(this).css('pointer-events','none');
				that.hidebubble = false;
				$('.skip_action').css('pointer-events','none');
				that.onClickSend();
			});

			const htmldata = $('<div class="message-container">').append(
			$('<div class="message type-1 passed">')
				.append(userDataDiv)
				.append(userSlider)
				.append(skip_btn)
			);
			htmldata.attr("id", "message-container" + this.count);
			$(".append").after(htmldata);
			$(".append").remove();
			$('<div class="append">').insertAfter(
			$("#message-container" + this.count)
			);
			$("#message-box").append($("#message-container"));

			this.count = this.count + 1;
		} else if (response.data.payload.hasOwnProperty("checkBox")) {
			var userCheckBox = $(
			'<div class="actions-container" id="actionContainer">'
			);
			var wrap = $('<div class="wrap">');
			var actions = $(
			'<div tabindex="1" class="expect-salary-action" id="actionButton-' +
			this.count +
			'" >'
			);
			var that = this;
			for (let i = 0; i < response.data.payload.checkBox.length; i++) {
			console.log("in loop " + response.data.payload.checkBox[i]);

			actions.append(
				$(
				'<div class="checkBoxContainer" style="display:flex;"><label class="label-container">' +
				response.data.payload.checkBox[i].key +
				'<input type="checkbox" class="ex-chckbox-input" id="checkBoxButton' +
				i +
				"-" +
				this.count +
				'" value="' +
				response.data.payload.checkBox[i].key +
				'"><span class="checkmark"></span>'
				)
			);

			$(document).on("click","#checkBoxButton" + i + "-" + this.count,function (event) {
				$('#checkbox-err').hide();
				that.hidebubble = true;
				if ($(this).is(":checked")) {
					console.log(this.value.toLowerCase());
					that.checkBoxResponse.push(this.value.toLowerCase());
					console.log(that.checkBoxResponse);
				}
			});
			this.count = this.count + 1;
			}
			wrap.append(actions);
			userCheckBox
			.append(wrap)
			.append($('<button type="button" class="action" id="checkbox-submission">Submit</button><span id="checkbox-err" style="display:none;margin-left: 15px;margin-top: 2px;color: #e3513f;">Please select any option first.</span>'));
			$(document).on("click", "#checkbox-submission", function () {
				if(that.checkBoxResponse.length > 0){
					that.userResponse = that.checkBoxResponse.join(",");
					$(this).css('pointer-events','none');
					that.hidebubble = false;
					$('.skip_action').css('pointer-events','none');
					that.onClickSend();
				}else{
					$('#checkbox-err').show();
				}
			});

			const htmldata = $('<div class="message-container">').append(
			$('<div class="message type-1 passed">')
				.append(userDataDiv)
				.append(userCheckBox)
				.append(skip_btn)
			);
			htmldata.attr("id", "message-container" + this.count);
			$(".append").after(htmldata);
			$(".append").remove();
			$('<div class="append">').insertAfter(
			$("#message-container" + this.count)
			);
			$("#message-box").append($("#message-container"));

			this.count = this.count + 1;
		} else if (response.data.payload.hasOwnProperty("uploadFiles")) {
			console.log("render upload box");
			var userFileBox = $(
			'<div class="actions-container" id="actionContainer">'
			);
			var that = this;
			for (let i = 0; i < response.data.payload.uploadFiles.length; i++) {
			console.log("in loop " + response.data.payload.uploadFiles[i]);
			userFileBox.append(
				$(' <div class="wrap">').append(
				$(
					'<div tabindex="1" class="expect-salary-action" id="actionButton' +
					i +
					"-" +
					this.count +
					'" >'
				).append(
					$(
					"<span>" +
					response.data.payload.uploadFiles[i].key +
					'</span> <input type="file" class="custom-file-input" id="fileInputButton' +
					i +
					"-" +
					this.count +
					'">'
					)
				)
				)
			);

			$(document).on(
				"change",
				"#fileInputButton" + i + "-" + this.count,
				function (event) {
				if (event.target.files && event.target.files[0]) {
					var reader = new FileReader();
					that.hidebubble = false;
					reader.readAsDataURL(event.target.files[0]);
					reader.onload = event => {
						var dataUrl = reader.result;
						that.uploadedFileBase64("sure", dataUrl, that);
					};
				}
				$('.skip_action').css('pointer-events','none');
				
				}
			);
			}

			const htmldata = $('<div class="message-container">').append(
			$('<div class="message type-1 passed">')
				.append(userDataDiv)
				.append(userFileBox)
				.append(skip_btn)
			);
			htmldata.attr("id", "message-container" + this.count);
			$(".append").after(htmldata);
			$(".append").remove();
			$('<div class="append">').insertAfter(
			$("#message-container" + this.count)
			);
			$("#message-box").append($("#message-container"));

			this.count = this.count + 1;
		} else if (response.data.payload.hasOwnProperty("formData")) {
			this.openAddDetailsDialog();
		} else if(response.data.payload.hasOwnProperty("end")){
			console.log("end");
			this.toggleButton = false;
			this.hidebubble = true;
			if(response.data.payload.end == 'success'){
				this.botSuccess = true;
			}else{
				this.botError = true;		
			}
		}else if (response.data.payload.hasOwnProperty("link")) {
			this.toggleButton = true;
			this.hidebubble = true;
			var userDataDiv = $('<div class="avatar-container" id="avatar">');
			var schedule_link = response.data.payload.link.url;
			userDataDiv.append(
			$('<div class="question" id="que">').append(
				$('<div class="text">')
				.append($('<p style="margin:0;"><a href=`${schedule_link}` target="_blank" style="color: #ffffff;>Schedule a meet</a></p>'))
				// .append($('<a style="margin:0;">').text(schedule_link))
				.append(
					$(
					'<span class="mat-time">' +
					this.time +
					' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'
					)
				)
			)
			);
			// for (let i = 0; i < response.data.text.length; i++) {
			//   userDataDiv.append(
			//     $('<div class="question" id="que">').append(
			//       $('<div class="text">')
			//         .append($('<p style="margin:0;">').text(response.data.text[i]))
			//         .append(
			//           $(
			//             '<span class="mat-time">' +
			//             this.time +
			//             ' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'
			//           )
			//         )
			//     )
			//   );
			// }
			const userText = document.querySelector("#userRes");
			const htmldata = $('<div class="message-container">').append(
			$('<div class="message type-1 passed">')
				.append(userDataDiv)
				.append(skip_btn)
			);
			htmldata.attr("id", "message-container" + this.count);
			$(".append").after(htmldata);
			$(".append").remove();
			// $('<div class="append">').insertAfter(
			//   $("#message-container" + this.count)
			// );
			this.count = this.count + 1;
			isTextPrinted = true;
		} else {
			this.hidebubble = true;
			if (!isTextPrinted) {
				const htmldata = $('<div class="message-container">').append(
					$('<div class="message type-1 passed">')
					.append(userDataDiv)
					.append(skip_btn)
				);
				htmldata.attr("id", "message-container" + this.count);
				$(".append").after(htmldata);
				$(".append").remove();
				$('<div class="append">').insertAfter(
					$("#message-container" + this.count)
				);
				$("#message-box").append($("#message-container"));
				this.count = this.count + 1;
			}
		}
	}
}

  buttonClick(text, that) {
	console.log("in fubn");
	that.hidebubble = false;
    var today = new Date();
    var amOrPm = today.getHours() < 12 ? "AM" : "PM";
    var time = today.getHours() + ":" + today.getMinutes() + " " + amOrPm;
    const enterdata = $('<div class="message-container right">').append(
      $('<div class="response-wrap" id="response-wrap">').append(
        $('<div class="response" id="response">').append(
          $('<div class="text" id="userTextSend">')
            .append($('<p style="margin:0;">').text(text))
            .append(
              $(
                '<span class="mat-time">' +
                time +
                ' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'
              )
            )
        )
      )
    );
    enterdata.attr("id", "right" + that.userCount);
    $(".append").after(enterdata);
    $(".append").remove();
    $('<div class="append">').insertAfter($("#right" + that.userCount));
    that.userCount = that.userCount + 1;
	console.log("click button " + text);
	that.userResponse = text;
	if(!that.apiRecalled){
		that.baseService.getResponse(that.sessionId, text).subscribe(
			data => {
				if(data['data'].text == ''){
					that.isResponseTextEmpty = true;
					that.apiRecalled = true;
					that.onClickSend();
				}else{
					that.hidebubble = true;
					that.isResponseTextEmpty = that.apiRecalled = false;
					that.generateHTML(data);
				}
			}
		);
	}else{
		this.botError = true;
		this.hidebubble = true;
	}
  }

  skipQuestion(that) {
    that.baseService.getResponse(that.sessionId, "skip").subscribe(data => {
	  	that.generateHTML(data);
		that.hidebubble = true;
	  
    });
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  uploadedFileBase64(text, dataUrl, that) {
    var today = new Date();
    var amOrPm = today.getHours() < 12 ? "AM" : "PM";
    var time = today.getHours() + ":" + today.getMinutes() + " " + amOrPm;
    const enterdata = $('<div class="message-container right">').append(
      $('<div class="response-wrap" id="response-wrap">').append(
        $('<div class="response" id="response">').append(
          $('<div class="text" id="userTextSend">')
            .append($('<p style="margin:0;">').text(text))
            .append(
              $(
                '<span class="mat-time">' +
                time +
                ' <mat-icon class="mat-icon material-icons mat-icon-no-color mat-custom-icon">check</mat-icon></span>'
              )
            )
        )
      )
    );
    enterdata.attr("id", "right" + that.userCount);
    $(".append").after(enterdata);
    $(".append").remove();
    $('<div class="append">').insertAfter($("#right" + that.userCount));
    that.userCount = that.userCount + 1;

    dataUrl = dataUrl.split(",");
    var base64String = dataUrl[1];
    var data = {
      clientid: CONFIG.jobDiva.clientid,
      username: CONFIG.jobDiva.username,
      password: CONFIG.jobDiva.password,
      filename: window.localStorage.getItem("cid") + ".doc",
      filecontent: base64String,
      candidateid: window.localStorage.getItem("cid"),
      resumesource: 4936,
      email: "",
      phone: ""
    };
    that.baseService.uploadCandidateResume(data).subscribe(
      response => {
        console.log(response);
        that.baseService.getResponse(that.sessionId, text).subscribe(data => {
          that.generateHTML(data);
        });
      },
      error => {
        console.log(error);
      }
    );
  }

  openAddCandidateDialog(): void {
    const dialogRef = this.dialog.open(AddCandidateComponent, {
      width: "500px",
      height: "fit-content"
    });
    dialogRef.afterClosed().subscribe(result => {
		console.log("The dialog was closed", result);
		this.emails.push({
				email: result.candidate_email,
				bot_url: ""
		});
		console.log('current route =>', this.activeRoute.url);
		let currentRoute = this.activeRoute.url;
		currentRoute = currentRoute.replace('undefined', window.localStorage.getItem("cid"));
		this.getRecruiterDetails(window.localStorage.getItem("rid"));
		const queryString = window.location.search;
		const urlParams = new URLSearchParams(queryString);
		const bd = urlParams.get('bd');
		if (bd == '2') {
			this.welcomeBdFunction(result)
		}
    });
  }

  welcomeBdFunction(formData) {
    var dataString = "Hi";
    var url = window.location.href;
    var queryString = url.split("?");
    var formDetails = formData;
    this.totalDetails = {
      formDetails: formDetails,
      recruiterDetails: this.recruiterDetails
    };
    this.addParameter = {
      name: "bddata",
      parameters: this.totalDetails
    }
    // this.getRecruiterByRid(this.ipObj.rid);
    if (queryString[1] != undefined)
      dataString = queryString[1].replace(/=/g, ": ").replace(/&/g, ", ");
    this.baseService
      .getFirstResponse(this.sessionId, dataString, this.addParameter)
      .subscribe(data => {
        this.showLoader = false;
        this.apiResponse = data;
        if (this.apiResponse.data.text != "") {
          this.generateHTML(this.apiResponse);
        } else {
          this.baseService.getFirstResponse(this.sessionId, dataString, this.addParameter).subscribe(data => {
            this.generateHTML(data);
          });
        }
      });
  }

	openAddDetailsDialog(): void {
		const dialogRef = this.dialog.open(AddDetailsComponent, {
			width: "500px",
			height: "fit-content"
		});
		dialogRef.afterClosed().subscribe(result => {
		console.log("The dialog was closed", result);
		this.emails.push({
			email: result.candidate_email,
			bot_url: ""
		});
		console.log('current route =>', this.activeRoute.url);
		this.welcomeDetailsFunction(result)
		});
	}

  welcomeDetailsFunction(formData) {
    var dataString = "Hi";
    var url = window.location.href;
    var queryString = "Formdata"
    var formDetails = formData;
    this.totalDetails = {
      formDetails: formDetails
    };
    this.addParameter = {
      name: "bddataThPTh",
      parameters: this.totalDetails
    }
    // this.getRecruiterByRid(this.ipObj.rid);
    dataString = queryString;
    this.baseService
      .getFirstResponse(this.sessionId, dataString, this.addParameter)
      .subscribe(data => {
        this.showLoader = false;
        this.apiResponse = data;
        if (this.apiResponse.data.text != "") {
          this.generateHTML(this.apiResponse);
        } else {
          this.baseService.getFirstResponse(this.sessionId, dataString, this.addParameter).subscribe(data => {
            this.generateHTML(data);
          });
        }
      });
  }
}