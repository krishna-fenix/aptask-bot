import { HomeComponent } from './home/home.component';
import { WelcomeWindowComponent } from './welcome-window/welcome-window.component';
import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

export const appRoutes: Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'welcome',
        component: WelcomeWindowComponent   
    },
    { path: '', redirectTo: 'home', pathMatch: 'full' }

];

export const mainAppRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);
