import { StoreValuesService } from './store-values.service';
import { CONFIG } from '../../config';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as RESTAPI from '../../assets/api-endpoint.json';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})

export class BaseService {
	urlStore: any;
	constructor(private http: HttpClient, public storeValues: StoreValuesService) {
		this.getJSON().subscribe(data => {
			this.storeValues.storeURL = data['end_point'];
			console.log(data);
		});
	}

	// getJSON() {
	//   return this.http.get('../../assets/api-endpoint.json').subscribe(data => {
	//     console.log('json data' + JSON.stringify(data));
	//     //  this.urlStore = data['point'];
	//     this.storeValues.storeURL = data['point'];
	//     console.log('url be' + this.storeValues.storeURL);
	//     return data['point'];
	//   });
	// }
	getJSON(): Observable<any> {
		return this.http.get('../../assets/api-endpoint.json');
	}

	getFirstResponse(sessionId, text, parameters) {
		console.log('sessionId' + sessionId + 'text' + text);
		const d = {
			sessionId,
			text,
			name: "data",
			contextData: parameters
		};
		console.log('fsd' + JSON.stringify(d));
		const options = { headers: { 'Content-Type': 'application/json' } };
		const body = new FormData();
		console.log('url after' + this.storeValues.storeURL);
		return this.http.post(this.storeValues.storeURL, d, options);
	}

	getResponse(sessionId, text) {
		console.log('sessionId' + sessionId + 'text' + text);
		const d = { sessionId, text };
		console.log('fsd' + JSON.stringify(d));
		const options = { headers: { 'Content-Type': 'application/json' } };
		const body = new FormData();
		console.log('url after' + this.storeValues.storeURL);
		return this.http.post(this.storeValues.storeURL, d, options);
	}

	getRecruiterDetails(rid): Observable<any> {
		const url = "http://pulse.aptask.com:7050/api/2.0/users/getuserbyid?userid=" + rid;
		const options = { headers: { 'Authorization': CONFIG.JWT_TOKEN } };
		return this.http.get(url, options)
	}

	getCandidateDetails(cand_id): Observable<any> {

		var url = "http://heallify.com:5001/api/v1/candJD/" + cand_id;

		console.log(url);

		return this.http.get(url);
	}

	uploadCandidateResume(body): Observable<any> {
		const options = { headers: { 'Authorization': CONFIG.JWT_TOKEN } };
		return this.http.post("http://pulse.aptask.com:7050/api/2.0/jobdiva/add-resume-to-jobdiva", body, options);
	}

	getRecruiterEmailList(): Observable<any> {
		const options = { headers: { 'Authorization': CONFIG.JWT_TOKEN } };
		return this.http.get('http://pulse.aptask.com:7050/api/2.0/users/list', options);
	}

	getCandidateIdBYEmail(email): Observable<any> {
		const url = "http://pulse.aptask.com:7050/api/2.0/jobdiva/searchCandidateByEmail?srchData=" + email + "&srchType=email";
		const options = { headers: { 'Authorization': CONFIG.JWT_TOKEN } };
		return this.http.get(url, options);
	}

	getJobIdByRefId(refId): Observable<any> {
		const url = "http://heallify.com:5001/api/v1/searchJob/" + refId;
		return this.http.get(url);
	}

	getShortenUrl(url): Observable<any> {
		const options = { headers: { 'Authorization': CONFIG.JWT_TOKEN } };
		const api_url = "http://pulse.aptask.com:7050/api/2.0/uploads/urlShortening";
		return this.http.post(api_url, { "url": url }, options);
	}

	addNewCandidate(formData): Observable<any> {
		const options = { headers: { 'Authorization': CONFIG.JWT_TOKEN } };
		const api_url = "http://pulse.aptask.com:7050/api/2.0/jobdiva/add-person-to-jobdiva";
		return this.http.post(api_url, formData, options);
	}

	addCandidateNote(data): Observable<any> {
		const options = { headers: { 'Authorization': CONFIG.JWT_TOKEN } };
		const api_url = "http://pulse.aptask.com:7050/api/2.0/jobdiva/add-candidate-note";
		return this.http.post(api_url, data, options);
	}

	sendBotToCandidate(data) {
		const options = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } };
		const url = "http://heallify.com:5001/api/v1/sendBot";
		return this.http.post(url, data);
	}

	tesrt() {
		return this.http.post('https://cyvh1io2qa.execute-api.ap-south-1.amazonaws.com/v1/abc', {});
	}

}
