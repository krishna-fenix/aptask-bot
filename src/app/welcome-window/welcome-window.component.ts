import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BaseService } from "../services/base.service";
import { CONFIG } from "../../config";
import { MatDialog } from "@angular/material";
import { COMMA, ENTER } from "@angular/cdk/keycodes";
import { MatChipInputEvent } from "@angular/material/chips";
import { EmailPreviewComponent } from "../email-preview/email-preview.component";
import { AddCandidateComponent } from "../add-candidate/add-candidate.component";
import * as RESTAPI from '../services/restapi';

@Component({
  selector: "app-welcome-window",
  templateUrl: "./welcome-window.component.html",
  styleUrls: ["./welcome-window.component.css"]
})
export class WelcomeWindowComponent implements OnInit {
  constructor(private baseService: BaseService, public dialog: MatDialog) { }

  welcomeForm: FormGroup;
  recruiterEmails: any;
  filteredEmails: any;
  showSubmitLoader: boolean = false;
  selectedEmails: any;
  botTypes = {
    "Contract W2 Only (GC/Citizen only)": 1,
    "Fulltime (H1B)": 2,
    "Fulltime (GC/Citizen only)": 3,
    "Contract W2 Only (H1B)": 4
  };
  botCategory = {
    "Recruiters": 1,
    "Business Developers": 2
  }
  toggleButtons: boolean = false;
  botUrl: any = "";
  formValidationMsg: string = "";
  formSuccessMsg: string = "";
  emailTemplates = CONFIG.EMAIL_TEMPLATES;
  selectedTemplate: any;
  showPreviewBtn: boolean = false;
  candidateData: any;
  jobData = {};
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  emails = [];
  candidates: any;
  optionValue: any = 1;

  ngOnInit() {
    console.log("on welcome page screen!!!");
    this.welcomeForm = new FormGroup({
      remail: new FormControl("", [Validators.required]),
      bot_category: new FormControl("", [Validators.required]),
      botType: new FormControl("", [Validators.required]),
      email_template: new FormControl("", [Validators.required]),
      // cemail: new FormControl('', [Validators.required,Validators.email]),
      jid: new FormControl("", [Validators.required])
    });
    this.getREmailList();
  }

  openTempletePreviewDialog(): void {
    const dialogRef = this.dialog.open(EmailPreviewComponent, {
      width: "500px",
      height: "fit-content",
      data: this.selectedTemplate
    });
  }

  openAddCandidateDialog(email): void {
    const dialogRef = this.dialog.open(AddCandidateComponent, {
      width: "500px",
      height: "fit-content",
      data: { email: email }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed", result);
      this.emails.push({
        email: result.candidate_email,
        bot_url: ""
      });
      // this.welcomeForm.get('cemail').setValue(result.candidate_email);
      // this.welcomeForm.value.cemail = result.candidate_email;
      // this.candidateData = result;
    });
  }

  displayFn(value): string {
    return value && value.email ? value.email : "";
  }

  onSelection(value) {
    console.log(value);
    this.selectedEmails = value;
  }

  filterEmails(value: string) {
    this.filteredEmails = this.searchEmails(value);
  }

  searchEmails(value: string) {
    let filter = value.toLowerCase();
    return this.recruiterEmails.filter(option =>
      option.name.toLowerCase().startsWith(filter)
    );
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    // Add our fruit
    if ((value || "").trim() && value.match(mailformat)) {
      this.checkEmailAvailability(value.trim());
      // this.emails.push({
      // 	email:value.trim(),
      // 	bot_url:''
      // });
    } else {
      this.formValidationMsg = "Please Enter Valid Email Id!!!";
    }

    // Reset the input value
    if (input) {
      input.value = "";
    }
    console.log(this.emails);
  }

  remove(email: any): void {
    const index = this.emails.indexOf(email);

    if (index >= 0) {
      this.emails.splice(index, 1);
    }
  }

  getREmailList() {
    this.baseService.getRecruiterEmailList().subscribe(
      response => {
        console.log(response);
        this.recruiterEmails = this.filteredEmails = response.data.result;
      },
      error => {
        console.log(error);
      }
    );
  }

  createBotUrl(formValue) {
    this.formValidationMsg = "";
    this.formSuccessMsg = "";
    this.candidates = this.emails;
    this.showSubmitLoader = true;
    if(formValue.remail.user_id == undefined){
      this.showSubmitLoader = false;
      this.formValidationMsg = "Recruiter is not verified! Please choose correct email id.";
    }
    else{
      if (this.optionValue == 1) {
      if (this.welcomeForm.valid) {
        // this.welcomeForm.get('cemail').reset();
        this.emails = [];
        this.toggleButtons = false;
        // this.botUrl = RESTAPI.API_ENDPOINT;
        var formData = formValue;
        var urlData = {
          rid: formData.remail.user_id,
          jid: formData.jid,
          bid: formData.botType,
          etid: formData.email_template.value.id
        };
        if (this.candidates.length > 0) {
          this.candidates.forEach((email, i) => {
            this.botUrl = RESTAPI.API_ENDPOINT;
            console.log("adsbaifbsaidufbisaduyfoycgfoysdgnoxfysgoxfyzgnsidyxgn", this.botUrl)
            this.baseService.getCandidateIdBYEmail(email.email).subscribe(
              response => {
                console.log(response);
                if (response.code === "409") {
                  this.formValidationMsg = response.message;
                } else {
                  // urlData.cid = response.data.CandID;
                  this.candidates[i]["firstname"] = response.data.FirstName;
                  this.candidates[i]["lastname"] = response.data.LastName;
                  this.searchJobID(urlData, response.data.CandID, i);
                }
              },
              error => {
                console.log(error);
                this.showSubmitLoader = false;
                this.formValidationMsg = error.message;
              }
            );
          });
        } else {
          this.candidates = [];
          var obj = {
            email: "",
            bot_url: ""
          };
          this.candidates.push(obj);
          this.searchJobID(urlData, undefined, 0);
          // this.formValidationMsg =
          //   "The form is invalid. Some fields are missing or invalid!!!";
        }
      } else {
        console.log("Form is invalid!!!");
        this.showSubmitLoader = false;
        this.formValidationMsg =
          "The form is invalid. Some fields are missing or invalid!!!";
      }
    } else {
      console.log(".....................");
      var formData = formValue;
      var bdUrlData = {
        rid: formData.remail.user_id,
        bd: formData.bot_category
      };
      this.createBusinessDeveloperUrl(bdUrlData, 0);
    }
    }
    
  }

  createBusinessDeveloperUrl(urlData, index) {
    var url = "";
    url =
      RESTAPI.API_ENDPOINT + "?rid=" +
      urlData.rid +
      "&bd=" +
      urlData.bd;
    this.shortenUrl(url, index);
  }

  shortenUrl(url, index) {
    this.baseService.getShortenUrl(url).subscribe(
      response => {
        console.log(response);
        this.showSubmitLoader = false;
        if (response.code === 200) {
          this.toggleButtons = true;
          // this.candidateData = null;
          this.botUrl = response.data.url;
          console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<", this.botUrl);
          // this.candidates[index].bot_url = this.botUrl;
          this.candidates = [];
          var obj = {
            email: "",
            bot_url: this.botUrl
          };
          this.candidates.push(obj);
          this.formSuccessMsg = "Url created Successfully!!!";
        } else {
          this.showSubmitLoader = false;
          this.formValidationMsg = "Url could not be shortened!!!";
        }
      },
      error => {
        console.log(error);
        this.showSubmitLoader = false;
      }
    );
  }

  openBot() {
    window.open(this.botUrl);
  }

  test() {
    this.baseService.tesrt().subscribe(response => {
      console.log(response);
    });
  }

  checkEmailAvailability(email) {
    this.formValidationMsg = "";
    this.baseService.getCandidateIdBYEmail(email).subscribe(
      response => {
        if (response.code === "409") {
          this.openAddCandidateDialog(email);
        } else {
          this.emails.push({
            email: email,
            bot_url: ""
          });
        }
      },
      error => {
        this.formValidationMsg = "Enter Email does not exists!!!";
      }
    );
  }

  copyLinkToClipboard(index) {
    var copyText = document.getElementById(
      "copy-clipboard-" + index
    ) as HTMLInputElement;
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand("copy");

    var tooltip = document.getElementById("myTooltip-" + index);
    tooltip.innerHTML = "Copied!!!";
  }

  outFunc(index) {
    var tooltip = document.getElementById("myTooltip-" + index);
    tooltip.innerHTML = "Copy to clipboard";
  }

  isNumberKey(evt) {
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
    return true;
  }

  changePreview(value) {
    console.log(value);
    this.showPreviewBtn = true;
    this.selectedTemplate = value;
  }

  getShortenUrl(url, index, cid) {
    this.baseService.getShortenUrl(url).subscribe(
      response => {
        console.log(response);
        this.showSubmitLoader = false;
        if (response.code === 200) {
          this.toggleButtons = true;
          // this.candidateData = null;
          this.botUrl = response.data.url;
          console.log(this.botUrl);
          if (cid == undefined) {
            console.log("<<<<<<<<<<<<<<<<<<hello>>>>>>>>>>>>>")
            this.candidates[index].bot_url = this.botUrl;
          } else {
            this.candidates[index].bot_url = this.botUrl;
            this.sendBot(index);
          }
          this.formSuccessMsg = "Url created Successfully!!!";
        } else {
          this.showSubmitLoader = false;
          this.formValidationMsg = "Url could not be shortened!!!";
        }
      },
      error => {
        console.log(error);
        this.showSubmitLoader = false;
      }
    );
  }

  searchJobID(urlData, cid, index) {
    var url = "";
    this.baseService.getJobIdByRefId(urlData.jid).subscribe(
      response => {
        console.log(response);
        if (response.data.respcode == "200") {
          url =
            RESTAPI.API_ENDPOINT + "?rid=" +
            urlData.rid +
            "&cid=" +
            cid +
            "&jid=" +
            response.data.respData.id +
            "&bid=" +
            urlData.bid +
            "&etid=" +
            urlData.etid;
          // this.candidates[index].bot_url = url;
          this.jobData["id"] = response.data.respData.id;
          this.jobData["JOBTITLE"] = response.data.respData["job title"];
          this.jobData["JOBDESCRIPTION"] = response.data.respData["job type"];
          if (cid == undefined) {
            url =
              RESTAPI.API_ENDPOINT + "?rid=" +
              urlData.rid +
              "&cid=" +
              cid +
              "&jid=" +
              response.data.respData.id +
              "&bid=" +
              urlData.bid +
              "&etid=" +
              urlData.etid;
            this.getShortenUrl(url, index, cid);
          }
          this.getShortenUrl(url, index, cid);
        } else {
          this.formValidationMsg =
            "Couldn't find the JobId from given JobDiva Reference #!!!";
          this.showSubmitLoader = false;
        }
      },
      error => {
        console.log(error);
        this.showSubmitLoader = false;
      }
    );
  }

  sendBot(index) {
    this.formSuccessMsg = "";
    this.formValidationMsg = "";
    var email = this.candidates[index];
      var formData = {
        url: email.bot_url,
        candidate: {
          firstname: email.firstname,
          lastname: email.lastname,
          email: email.email
        },
        recruiter: {
          firstname: this.selectedEmails.first_name,
          lastname: this.selectedEmails.last_name,
          mobile: this.selectedEmails.mobile_no,
          email: this.selectedEmails.email
        },
        job: this.jobData,
        etid: this.selectedTemplate.value.id
      };

      console.log(formData);
      this.baseService
        .sendBotToCandidate({ data: JSON.stringify(formData) })
        .subscribe(
          response => {
            console.log(response);
            if (response["success"] == "true")
              this.formSuccessMsg = "Bot sent to candidate successfully!!!";
            else this.formValidationMsg = "Couldn't send bot to candidate!!!";
          },
          error => {
            console.log(error);
            this.formValidationMsg = "Couldn't send bot to candidate!!!";
          }
        );
  }
}
