import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BaseService } from "../services/base.service";
import * as $ from "jquery";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { CONFIG } from "../../config";

@Component({
  selector: "app-add-candidate",
  templateUrl: "./add-candidate.component.html",
  styleUrls: ["./add-candidate.component.css"]
})
export class AddCandidateComponent implements OnInit {
  addCandidateForm: FormGroup;
  formValidationMsg: string = "";
  formSuccessMsg: string = "";
  emailCheck: boolean = false;
  invalidEmail: boolean = false;
  validEmail: boolean = false;
  showLoader: boolean = false;
  emailAvailable: boolean;
  candidate_email: string = (this.data != null) ? this.data.email : '';
  constructor(
		public dialogRef: MatDialogRef<AddCandidateComponent>,
		private baseService: BaseService,
		@Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  	ngOnInit() {
		this.addCandidateForm = new FormGroup({
			first_name: new FormControl("", [Validators.required]),
			last_name: new FormControl("", [Validators.required]),
			email: new FormControl(this.candidate_email, [Validators.required, Validators.email]),
			cell_phone: new FormControl("")
		});
    	console.log("<<<<<<<<<<<<<<", this.candidate_email)
  	}

	onClose() {
		this.dialogRef.close();
	}

	isNumberKey(evt) {
		var charCode = evt.which ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
		return true;
	}

  // checkEmailAvailability() {
  //   this.formValidationMsg = "";
  //   if (this.addCandidateForm.value.email) {
  //     this.emailCheck = true;
  //     this.showLoader = true;
  //     this.baseService.getCandidateIdBYEmail(this.addCandidateForm.value.email).subscribe(response => {
  //       if (response.code === "409") {
  //         this.emailAvailable = true;
  //       } else {
  //         this.emailAvailable = false;
  //         const candidate_id = response.data.CandID;
  //         console.log("candidate id => ", candidate_id);
  //         window.localStorage.setItem("cid", candidate_id);
  //       }
  //     },
  //       error => {
  //         console.log(error);
  //       }
  //     );
  //   } else {
  //     this.formValidationMsg = "Please Enter Email Id!!!";
  //   }
  // }

  showLink() {
    this.emailCheck = false;
    this.showLoader = false;
    this.validEmail = false;
    this.invalidEmail = false;
  }

  addNewCandidate(formValue) {
    console.log(formValue);
    // this.checkEmailAvailability();
    this.formValidationMsg = "";
    this.formSuccessMsg = "";
    this.formValidationMsg = "";
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const bd = urlParams.get('bd');

    if (bd == undefined) {
      if (this.addCandidateForm.value.email) {
        this.emailCheck = true;
        this.showLoader = true;
        this.baseService.getCandidateIdBYEmail(this.addCandidateForm.value.email).subscribe(response => {
          if (response.code === "409") {

            if (this.addCandidateForm.valid) {
              var formData = formValue;
              this.baseService.addNewCandidate(formData).subscribe(
                response => {
                  console.log(response);
                  this.formSuccessMsg = "Candidate Created Successfully!!!";
                  var responseData = {
                    candidate_email: formData.email,
                    cand_id: "",
                    firstname: formData.first_name,
                    lastname: formData.last_name
                  };
                  const { children } = response.data.children[0];
                  const [candidate] = children.filter(c => c.tag === "candidateid");
                  const { children: child } = candidate;
                  responseData.cand_id = child[0].text;
                  console.log(responseData.cand_id);
                  window.localStorage.setItem("cid", responseData.cand_id);
                  console.log("candidate id", responseData.cand_id);
                  this.dialogRef.close(responseData);
                  this.addCandidateNote();
                },
                error => {
                  console.log(error);
                  this.dialogRef.close(null);
                }
              );
            } else {
              console.log("form is invalid!!!");
              this.formValidationMsg = "Form is Invalid!!!";
            }
          } else {

            // this.emailAvailable = false;

            const candidate_id = response.data.CandID;
            console.log("candidate id => ", candidate_id);
            window.localStorage.setItem("cid", candidate_id);

            var formData = formValue;
            var responseData = {
              candidate_email: formData.email
            };
            this.dialogRef.close(responseData);
            this.addCandidateNote();

          }
        },
          error => {
            console.log(error);
          }
        );
      } else {
        this.formValidationMsg = "Please Enter Email Id!!!";
      }
    } else {
      var formData = formValue;
      this.dialogRef.close(formData);
    }

    // if (this.emailAvailable == true) {
    // console.log("here");
    // if (this.addCandidateForm.valid) {
    //   var formData = formValue;
    //   this.baseService.addNewCandidate(formData).subscribe(
    //     response => {
    //       console.log(response);
    //       this.formSuccessMsg = "Candidate Created Successfully!!!";
    //       var responseData = {
    //         candidate_email: formData.email,
    //         cand_id: "",
    //         firstname: formData.first_name,
    //         lastname: formData.last_name
    //       };
    //       const { children } = response.data.children[0];
    //       const [candidate] = children.filter(c => c.tag === "candidateid");
    //       const { children: child } = candidate;
    //       responseData.cand_id = child[0].text;
    //       console.log(responseData.cand_id);
    //       window.localStorage.setItem("cid", responseData.cand_id);
    //       console.log("candidate id", responseData.cand_id);
    //       this.dialogRef.close(responseData);
    //       this.addCandidateNote();
    //     },
    //     error => {
    //       console.log(error);
    //       this.dialogRef.close(null);
    //     }
    //   );
    // } else {
    //   console.log("form is invalid!!!");
    //   this.formValidationMsg = "Form is Invalid!!!";
    // }
    // } else if (this.emailAvailable == false) {
    //   var formData = formValue;
    //   var responseData = {
    //     candidate_email: formData.email
    //   };
    //   this.dialogRef.close(responseData);
    //   this.addCandidateNote();
    // }
  }

  addCandidateNote() {
    let candData =
    {
      "candidateJobDivaId": window.localStorage.getItem("cid"),
      "note": "Conversation with candidate through bot started on <dateTime>"
    }

    this.baseService.addCandidateNote(candData).subscribe(() => {
    });
  }
}
