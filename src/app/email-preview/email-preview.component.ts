import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
	selector: 'app-email-preview',
	templateUrl: './email-preview.component.html',
	styleUrls: ['./email-preview.component.css']
})
export class EmailPreviewComponent implements OnInit {

	constructor(
		public dialogRef: MatDialogRef<EmailPreviewComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any

	) { }

	ngOnInit() {
		console.log(this.data);
	}

	onClose() {
		this.dialogRef.close();
	}


}
