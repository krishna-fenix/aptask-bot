import { Component, OnInit } from "@angular/core";
import { BaseService } from "./../services/base.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  public chatWindow = false;
  constructor(private baseService: BaseService) { }

  RName: string;
  REmail: string;
  RMobile: string;
  RTwillio: string;
  RProfileImage: string;
  linkRefTitle: string = "Not available";
  RId: any;
  linkedin: string = "";
  calender: string = "";
  videoIntro: string = "";

  ngOnInit() {
    window.localStorage.clear();
    var urlParams = new URLSearchParams(window.location.search);
    this.RId = urlParams.get("rid");
    this.RProfileImage = "../../assets/images/download.jpeg";
    window.localStorage.setItem("rid", urlParams.get("rid"));
    window.localStorage.setItem("jid", urlParams.get("jid"));
    window.localStorage.setItem("cid", urlParams.get("cid"));
    window.localStorage.setItem("bid", urlParams.get("bid"));
    window.localStorage.setItem("etid", urlParams.get("etid"));
    this.getRecruiterByRid(this.RId);
  }

  openChatWindow() {
    this.chatWindow = true;
  }
  closeChatWindow() {
    this.chatWindow = false;
  }

  getRecruiterByRid(rid) {
    console.log("call recruiter...");
    this.baseService.getRecruiterDetails(rid).subscribe(
      data => {
        console.log("-----Recruiter Success----");
        console.log(data.data);
        this.RName = data.data.name;
        this.REmail = data.data.email;
        this.RMobile = this.formatPhoneNumber(data.data.voip_number);
        this.RProfileImage = data.data.avatar;
        this.RTwillio = this.formatPhoneNumber(data.data.sms_number_twillio);
        this.linkedin = data.data.social_link;
        this.calender = data.data.calendly_link;
        this.videoIntro = data.data.video_intro_url;
      },
      error => {
        console.log("-----Recruiter Errror----");
        console.log(error);
      }
    );
  }

  openUrl(url) {
    if (url) window.open(url);
  }

  formatPhoneNumber(phoneNumberString) {
    var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
    var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      var intlCode = match[1] ? "+1 " : "";
      return [intlCode, "(", match[2], ") ", match[3], "-", match[4]].join("");
    }
    return "--";
  }
}
