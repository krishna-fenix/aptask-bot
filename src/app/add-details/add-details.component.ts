import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { AddCandidateComponent } from '../add-candidate/add-candidate.component';
import { BaseService } from '../services/base.service';

@Component({
  selector: 'app-add-details',
  templateUrl: './add-details.component.html',
  styleUrls: ['./add-details.component.css']
})
export class AddDetailsComponent implements OnInit {

  addDetailsForm: FormGroup;
  formValidationMsg: string = "";
  formSuccessMsg: string = "";
  emailCheck: boolean = false;
  invalidEmail: boolean = false;
  validEmail: boolean = false;
  showLoader: boolean = false;
  emailAvailable: boolean;
  constructor(
    public dialogRef: MatDialogRef<AddCandidateComponent>,
    private baseService: BaseService
  ) { }

  ngOnInit() {
    this.addDetailsForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      // last_name: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required, Validators.email]),
      number: new FormControl("", [Validators.required]),
      title: new FormControl("", [Validators.required]),
      comments: new FormControl("")
    });
  }

  onClose() {
    this.dialogRef.close();
  }

  isNumberKey(evt) {
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
    return true;
  }

  addDetails(formValue) {
    console.log(formValue);
    this.formValidationMsg = "";
    this.formSuccessMsg = "";
    this.formValidationMsg = "";
    var formData = formValue;
    // const queryString = window.location.search;
    // const urlParams = new URLSearchParams(queryString);
    // const bd = urlParams.get('bd');

    // if (bd == undefined) {
    //   if (this.addCandidateForm.value.email) {
    //     this.emailCheck = true;
    //     this.showLoader = true;
    //     this.baseService.getCandidateIdBYEmail(this.addCandidateForm.value.email).subscribe(response => {
    //       if (response.code === "409") {

    //         if (this.addCandidateForm.valid) {
    //           var formData = formValue;
    //           this.baseService.addNewCandidate(formData).subscribe(
    //             response => {
    //               console.log(response);
    //               this.formSuccessMsg = "Candidate Created Successfully!!!";
    //               var responseData = {
    //                 candidate_email: formData.email,
    //                 cand_id: "",
    //                 firstname: formData.first_name,
    //                 lastname: formData.last_name
    //               };
    //               const { children } = response.data.children[0];
    //               const [candidate] = children.filter(c => c.tag === "candidateid");
    //               const { children: child } = candidate;
    //               responseData.cand_id = child[0].text;
    //               console.log(responseData.cand_id);
    //               window.localStorage.setItem("cid", responseData.cand_id);
    //               console.log("candidate id", responseData.cand_id);
    //               this.dialogRef.close(responseData);
    //             },
    //             error => {
    //               console.log(error);
    //               this.dialogRef.close(null);
    //             }
    //           );
    //         } else {
    //           console.log("form is invalid!!!");
    //           this.formValidationMsg = "Form is Invalid!!!";
    //         }
    //       } else {

    //         // this.emailAvailable = false;

    //         const candidate_id = response.data.CandID;
    //         console.log("candidate id => ", candidate_id);
    //         window.localStorage.setItem("cid", candidate_id);

    //         var formData = formValue;
    //         var responseData = {
    //           candidate_email: formData.email
    //         };
    //         this.dialogRef.close(responseData);
    //       }
    //     },
    //       error => {
    //         console.log(error);
    //       }
    //     );
    //   } else {
    //     this.formValidationMsg = "Please Enter Email Id!!!";
    //   }
    // } else {
    //   var formData = formValue;
    //   this.dialogRef.close(formData);
    // }

    this.dialogRef.close(formData)
  }

}
