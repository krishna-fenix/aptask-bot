import { StoreValuesService } from './services/store-values.service';
import { BaseService } from './services/base.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ChatWindowComponent } from './chat-window/chat-window.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import {

  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatTableModule,
  MatProgressSpinnerModule,
  MatTooltipModule
} from '@angular/material';
import { MatChipsModule } from '@angular/material/chips';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { WelcomeWindowComponent } from './welcome-window/welcome-window.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routing';
import { EmailPreviewComponent } from './email-preview/email-preview.component';
import { SanitizeHtmlPipe } from './sanitize-html.pipe';
import { AddCandidateComponent } from './add-candidate/add-candidate.component';
import { AddDetailsComponent } from './add-details/add-details.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ChatWindowComponent,
    WelcomeWindowComponent,
    EmailPreviewComponent,
    SanitizeHtmlPipe,
    AddCandidateComponent,
    AddDetailsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatChipsModule,
    MatIconModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatTooltipModule
  ],
  entryComponents: [
    EmailPreviewComponent,
    AddCandidateComponent,
    AddDetailsComponent
  ],
  providers: [
    BaseService,
    StoreValuesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
